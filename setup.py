from setuptools import setup, find_packages
from os import path, getenv


from io import open

here = path.abspath(path.dirname(__file__))


with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()


setup(
    name="rgs-pylogger",
    description="Edited loguru",
    long_description=long_description,
    long_description_content_type="text/markdown",
    version="1.1.0",
    url=getenv("CI_JOB_URL"),
    author=getenv("GITLAB_USER_NAME"),
    author_email=getenv("GITLAB_USER_EMAIL"),
    license="MIT",
    packages=find_packages(include=["PyLogger", "PyLogger.*"]),
    install_requires=[
        "colorama==0.4.4; sys_platform == 'win32'",
        "loguru==0.5.3",
        "win32-setctime==1.0.3; sys_platform == 'win32'",
    ],
    keywords=["", ""],
)
