import os
import sys
import types

import loguru

CI_DEBUG_TRACE = os.getenv("CI_DEBUG_TRACE", "false")
LOGGING_LEVEL = "INFO" if CI_DEBUG_TRACE == "false" else "DEBUG"

loguru.logger.remove()

loguru.logger.add(
    sys.stdout,
    colorize=True,
    format="<lvl>{message}</lvl>",
    level=LOGGING_LEVEL,
)

loguru.logger.add(
    lambda _: sys.exit(1),
    colorize=True,
    format="<lvl>{message}</lvl>",
    level="ERROR",
)

loguru.logger.level("MESSAGE", no=25)
loguru.logger.level("INFO", color="<blue>")
loguru.logger.level("DEBUG", color="<yellow>")
loguru.logger.level("ERROR", color="<red><bold>")


def message(self, message):
    self.log("MESSAGE", message)
msg = message

def framed_log(
    self,
    log_sl: str = "INFO",
    message: str = "",
    title: str = "",
    frame_size: int = 75,
    frame_type: str = "=",
) -> None:
    log_lvl = log_sl.upper()
    _frame = frame_type * frame_size
    _nl = "\n"
    if title:
        _frame_side = frame_type * ((frame_size - len(title)) // 2)
        _title_frame = f"{_frame_side} {title} {_frame_side}"
        _bottom_frame = frame_type * len(_title_frame)
        _message = f"{_title_frame}{_nl}{message}{_nl}{_bottom_frame}{_nl}"
    else:
        _message = f"{_frame}{_nl}{message}{_nl}{_frame}"
    self.log(log_lvl, _message)


loguru.logger.message = types.MethodType(message, loguru.logger)
loguru.logger.msg = types.MethodType(msg, loguru.logger)
loguru.logger.framed_log = types.MethodType(framed_log, loguru.logger)


logger = loguru.logger
_Logger = logger
