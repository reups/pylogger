# PyLogger

Небольшая надстройка над Loguru

## Usage
```python
from PyLogger import logger
```

Команда | Описание | Цвет
---  | ---  | ---  
logger.message("message") | Обычный вывод сообщения | белый
logger.info("info") | Вывод информационных сообщений | синий
logger.debug("debug") | Вывод текста, если переменная окружения CI_DEBUG_TRACE = "true" | жёлтый
logger.warning("warning") | Вывод предупреждающих сообщений | жёлтый
logger.error("error") | Вывод ошибки, с последующим выходом sys.exit(1) | красный

1. logger.framed_log(message="message", title="title")
<br> Обертка сообщения в рамки.
<br> Дополнительные переменные:
- log_sl: str = ("INFO") -  severity_level сообщения
- message: str = "" -  текст сообщения
- title: str = "" -  заголовок
- frame_size: int = 75 -  размер рамки
- frame_type: str = "=" -  вид рамки